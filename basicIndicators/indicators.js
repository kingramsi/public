/*

  Description:
  Very basic JohnnyFive script to toggle my indicators on and off.
  
  Framework:
  https://github.com/rwaldron/johnny-five

*/


var five = require("johnny-five");
var board = new five.Board();

board.on("ready", function() {

  // VARIABLES
  var SwitchLeftIndicator = new five.Button(6);
  var SwitchRightIndicator = new five.Button(7);
  var leftIndicator = new five.Led(9);
  var rightIndicator = new five.Led(10);

  // LEFT INDICATOR
  SwitchLeftIndicator.on("hold", function() {
    console.log( "flash left" );
    leftIndicator.blink(200);
  });

  SwitchLeftIndicator.on("release", function() {
    console.log( "indicator off" );
    leftIndicator.stop().off();
  });

  // RIGHT INDICATOR
  SwitchRightIndicator.on("hold", function() {
    console.log( "flash right" );
    rightIndicator.blink(200);
  });

  SwitchRightIndicator.on("release", function() {
    console.log( "indicator off" );
    rightIndicator.stop().off();
  });


});